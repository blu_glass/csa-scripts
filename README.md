# CSA-scripts

This project contains all the prepossessing scripts that are required in order to analyze  
CSA data.

# prerequisites
**Please make sure you have a gpu on you machine**  

On a clean machine, you will need to run the install.sh bash script in order have the prerequisites needed for this project  
First run the following command:
```
$: chmod -R +x ./scripts
```
Then run the following bash scripts by:
```
$: sudo bash ./scripts/install_docker.sh
$: sudo bash ./scripts/install.sh
```
you will see a prompt asking you which dependencies to install. 
you should install all of them if they does not exist already.


### Configuration
In the project folder you have a csa_config.ini file which holds the project configuration.
You can modify the database, models and elasticsearch indexes names  

### usage
To deploy the docker images run:
run the bash script by:
```
$: ./scripts/deploy_docker_images.sh
```
and activate python virtual environment by:
```
$: source ./venv/bin/activate
```

This project contains three main scripts
1. Import project vic - this will allow you to see summaries of project vic at http://localhost:5601/app/kibana#/dashboards
 as well as to find matches to project vic within the images you will process
```
$: python src/cli.py import-vic --path <path to project vic json>
```
2. Process images - process folder containing images. this will do the folowing:
    * create an es index of the images
    * search for matches in project vic and can
    * apply one of the available processing steps - faces detection, age/gender estimation (requires face detection first),
    image classification and feature extraction  (it is possible to run them all at once with -p all, or select few such as -p age -p gender)
```
$: python src/cli.py apply-process --path <path to folder> --index <elastic index name> -p <age/gender/face/classification/fv/all>
```
3. export training catalogue - this api will create a .csv file with file path and corresponding class.
 you can choose to csa estimation class (binary), or choose vic categories (multiclass) for the files that was matched in project vic    
```
$: python src/cli.py export-catalogue --export_path <export path for the catalogue> --index <elastic index name> -c <vic/estimate>
```

after processing the data you can access http://localhost:5601/app/kibana#/dashboards and see the data summary and insights.
(to enable all visualizations, on kibana sidebar go to Managment->Nested Fields, and check all the raws)

# Contact
In any case of  a problem, you are welcome to send an email [Adiel Cahana](mailto:adiel.cahana@cellebrite.com)

