#!/usr/bin/env bash

apt-get install software-properties-common python-software-properties
#Installs python 3.6
add-apt-repository -y ppa:jonathonf/python-3.6
add-apt-repository -y ppa:deadsnakes/ppa

apt update
apt install -y python3.6
ln -s -f /usr/bin/python3.6 /usr/bin/python3

wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
rm get-pip.py

apt-get install python-dev3.6