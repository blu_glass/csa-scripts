### sources
# download the following models to the models folders:
# for age and gender models:
# models source and code is: https://github.com/dpressel/rude-carnie
# - download https://drive.google.com/drive/folders/0B8N1oYmGLVGWbDZ4Y21GLWxtV1E and rename the folder to age
# - download https://drive.google.com/drive/folders/0B8N1oYmGLVGWemZQd3JMOEZvdGs and rename the folder to gender
# for face detection and landmarks:
# - under models folder, create face folder and download the following models to this folder
# - download http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2 (can't be used for commercial use)
# - download https://github.com/yeephycho/tensorflow-face-detection/raw/master/model/frozen_inference_graph_face.pb

apt-get install unzip

curl -LO https://nexus-forensic.cellebrite.com/repository/thirdparty/com/cellebrite/analytics/csa_scripts/photodna/0.0.0.1/photodna-0.0.0.1.zip
unzip photodna-0.0.0.1.zip -d resources/bin/
rm photodna-0.0.0.1.zip

curl -LO https://nexus-forensic.cellebrite.com/repository/thirdparty/com/cellebrite/analytics/csa_scripts/image_classification/0.0.0.1/image_classification-0.0.0.1.zip
unzip image_classification-0.0.0.1.zip -d resources/models/
rm image_classification-0.0.0.1.zip

curl -LO https://nexus-forensic.cellebrite.com/repository/thirdparty/com/cellebrite/analytics/csa_scripts/gender/0.0.0.1/gender-0.0.0.1.zip
unzip gender-0.0.0.1.zip -d resources/models/
rm gender-0.0.0.1.zip

curl -LO https://nexus-forensic.cellebrite.com/repository/thirdparty/com/cellebrite/analytics/csa_scripts/face/0.0.0.1/face-0.0.0.1.zip
unzip face-0.0.0.1.zip -d resources/models/
rm face-0.0.0.1.zip

curl -LO https://nexus-forensic.cellebrite.com/repository/thirdparty/com/cellebrite/analytics/csa_scripts/age/0.0.0.1/age-0.0.0.1.zip
unzip age-0.0.0.1.zip -d resources/models/
rm age-0.0.0.1.zip




