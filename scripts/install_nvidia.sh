#!/usr/bin/env bash

# Add NVIDIA package repositories
wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1604/x86_64/nvidia-machine-learning-repo-ubuntu1604_1.0.0-1_amd64.deb
apt install -y ./nvidia-machine-learning-repo-ubuntu1604_1.0.0-1_amd64.deb
apt-get update

# Install NVIDIA driver
# Issue with driver install requires creating /usr/lib/nvidia
apt-get install -y --no-install-recommends nvidia-410 nvidia-modprobe nvidia-settings

rm nvidia-machine-learning-repo-ubuntu1604_1.0.0-1_amd64.deb