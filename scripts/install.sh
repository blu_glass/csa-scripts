#!/usr/bin/env bash

yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "cannot $*"; }


read -p "Do you want to install nvidia drivers? [y/n] " -n 1 -r nvidia
echo    # (optional) move to a new line
read -p "Do you want to install cuda 10? [y/n] " -n 1 -r cuda
echo    # (optional) move to a new line
read -p "Do you want to install python? [y/n] " -n 1 -r python
echo    # (optional) move to a new line
read -p "Do you want to create virtual enviroment and install depndencies? [y/n] " -n 1 -r venv
echo    # (optional) move to a new line
read -p "Do you want to download resources (models, bins etc.)? [y/n] " -n 1 -r resources
echo    # (optional) move to a new line


# Add HTTPS support for apt-key
apt-get install -y gnupg-curl

# increase max virtual memory for elastic search
if [[ ! $(sysctl -n vm.max_map_count) = "262144" ]]
then
  echo "vm.max_map_count=262144" >> /etc/sysctl.conf
  sysctl -p
fi

if [[ $nvidia =~ ^[Yy]$ ]]
then
  echo "installing nvidia drivers"
  . scripts/install_nvidia.sh
fi

if [[ $cuda =~ ^[Yy]$ ]]
then
  echo "installing cuda 10 toolkit"
  . scripts/install_cuda_10.sh
fi

if [[ $python =~ ^[Yy]$ ]]
then
  echo "installing python 3.6"
  . scripts/install_python.sh
fi

if [[ $venv =~ ^[Yy]$ ]]
then
  echo "creating virtaul environment & installing python packages"
  sudo -u $SUDO_USER bash scripts/create_venv.sh
fi

if [[ $resources =~ ^[Yy]$ ]]
then
  echo "downloading resources"
  sudo -u $SUDO_USER bash scripts/download_resources.sh
fi


