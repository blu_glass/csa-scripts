#!/usr/bin/env bash

# install ijson-yajl backend
apt-get install libyajl2

python3 -m pip install -U virtualenv

echo
echo "Setting up venv"

if ! [ -d "venv" ]; then
    python3 -m virtualenv --system-site-packages ./venv
else
    echo "venv already exists!"
fi

"export ${PWD}/src" >> venv/bin/activate
source ./venv/bin/activate

REQUIREMENTS_PATH="requirements.txt"
echo "REQUIREMENTS_PATH is set to ${REQUIREMENTS_PATH}"
python -m pip install -r ${REQUIREMENTS_PATH}
