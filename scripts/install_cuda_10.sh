#!/usr/bin/env bash

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_10.0.130-1_amd64.deb
dpkg -i cuda-repo-ubuntu1604_10.0.130-1_amd64.deb
apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
apt-get update
# Install development and runtime libraries (~4GB)
apt-get install -y --no-install-recommends \
    cuda-10-0 \
    libcudnn7  \
    libcudnn7-dev

rm cuda-repo-ubuntu1604_10.0.130-1_amd64.deb

