#!/usr/bin/env bash

if [ ! -d "data" ]; then
  mkdir data
fi

if [ ! -d "data/es_data" ]; then
  mkdir data/es_data
fi

if [ ! -d "data/nginx-config" ]; then
  mkdir data/nginx-config
fi

cd docker
docker-compose up -d
docker exec -ti kibana sh -c "bin/kibana-plugin install https://github.com/ppadovani/KibanaNestedSupportPlugin/releases/download/6.4.2-1.0.2/nested-fields-support-6.4.2-1.0.2.zip"
docker restart kibana
cd ..