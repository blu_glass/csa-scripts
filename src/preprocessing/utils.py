import sys
import hashlib
from multiprocessing import Process, Queue
from functools import wraps

from tblib import pickling_support
pickling_support.install()

import pandas as pd
from PIL import Image

# MAGIC signatures
IMAGE_SIGNATURES = (bytes.fromhex("FFD8FF"),
                    # png
                    bytes.fromhex("89504E470D0A1A0A"),
                    # bmp
                    bytes.fromhex("424D")
                    )


def md5(fname):
    """
    gets file md5
    :param fname:
    :return:
    """
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def check_if_image(file_binary):
    """
    Checks if a file is an image
    :param file_binary:
    :return:
    """
    flag = False
    for sig in IMAGE_SIGNATURES:
        if sig == file_binary[:len(sig)]:
            flag = True
    return flag


def verify_image(img_file):
    """
    Checks if an image is corrupted
    :param img_file:
    :return:
    """
    # test image
    try:
        v_image = Image.open(img_file)
        v_image.verify()
        return False
        # is valid
        # print("valid file: "+img_file)
    except OSError:
        return True


def preproc_resize(img_path, size):
    img = Image.open(img_path)
    wpercent = (size/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((size, hsize), Image.ANTIALIAS)
    img.save('resized.jpg')


def convert_gen_to_dataframe(images_path_gen):
    df = pd.DataFrame(images_path_gen, columns=["filename"])
    # mock label column for keras method flow_from_dataframe
    df['label'] = '0'
    return df


class ExceptionWrapper(object):
    def __init__(self, ee):
        self.ee = ee
        __,  __, self.tb = sys.exc_info()

    def re_raise(self):
        raise self.ee.with_traceback(self.tb)


def subprocess(f):
    @wraps(f)
    def error_tracker(q, *args, **kwargs):
        try:
            result = f(*args, **kwargs)
            q.put(result)
        except Exception as e:
            e = ExceptionWrapper(e)
            q.put(e)

    @wraps(error_tracker)
    def wrapper(*args, **kwargs):
        q = Queue()
        p = Process(target=error_tracker, args=(q, *args), kwargs=kwargs)
        p.start()
        p.join()

        if not q.empty():
            result = q.get()
            if type(result) == ExceptionWrapper:
                result.re_raise()
            return result

    return wrapper