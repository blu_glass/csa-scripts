from concurrent.futures import ThreadPoolExecutor
from functools import partial

from elasticsearch import Elasticsearch as ES
import pandas as pd

from PhotoDNA.calc_photodna import calculate_photo_dna
from kibana.kibana import create_dashboard
from ml.ageGender.age_gender import get_age_gender
from ml.classification import infer
from ml.face.face_detect import get_faces_bb
from preprocessing.utils import verify_image, md5, subprocess
from preprocessing.es_utils import *

from config.consts import *
from config.resources import *

import os
import logging

from project_vic.config import VIC_CATEGORIES_MAP

logger = logging.getLogger("csa")

MAPPING = {
    "mappings": {
        "doc": {
            "properties": {
                "path": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "name": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                IMAGE_CLASSIFICATION: {
                    "type": "nested",
                    "properties": {
                        "model": {"type": "keyword"},
                        "category": {"type": "keyword"},
                        "prob": {"type": "float"},
                    }
                },
                FEATURE_EXTRACTION: {
                    "type": "nested",
                    "properties": {
                        "model": {"type": "keyword"},
                        FEATURE_EXTRACTION: {"type": "float"}
                    }
                },
                FACES: {
                    "type": "nested",
                    "properties": {
                        "left": {"type": "integer"},
                        "top": {"type": "integer"},
                        "right": {"type": "integer"},
                        "bottom": {"type": "integer"}
                    }
                },
                AGES: {
                    "type": "nested",
                    "properties": {
                        "age": {"type": "keyword"},
                        "prob": {"type": "float"},
                        "group": {"type": "keyword"}
                    }
                },
                GENDERS: {
                    "type": "nested",
                    "properties": {
                        "gender": {"type": "keyword"},
                        "prob": {"type": "float"}
                    }
                },
                "is_corrupted": {"type": "boolean"},
                "vic_info": {
                    "type": "object",
                    "properties": {
                        "in_project_vic": {"type": "boolean"},
                        "category": {"type": "keyword"}
                    }
                },
                "Hash": {
                    "type": "object",
                    "properties": {
                        "MD5": {"type": "keyword"},
                        "PhotoDNA": {"type": "keyword"}
                    }
                }
            }
        }
    }
}


def process_file(file_path, es_client, project_vic_index):
    photo_dna = calculate_photo_dna(file_path)
    md5_val = md5(file_path)

    # runs basic sanity checks on the image file
    vic_dict = match_to_project_vic(file_md5=md5_val,
                                    file_photodna=photo_dna,
                                    es_client=es_client,
                                    project_vic_index=project_vic_index)

    # generates the result json
    result = {
        "name": os.path.basename(file_path),
        "path": file_path,
        "Hash": {
            "MD5": md5_val,
            "PhotoDNA": photo_dna
        },
        "vic_info": vic_dict,
        IMAGE_CLASSIFICATION: [],
        FEATURE_EXTRACTION: [],
        "is_corrupted": verify_image(file_path)
    }

    return result


def create_index(folder_path, es_client, vic_index_name, folder_index):
    es_client.indices.create(index=folder_index, body=MAPPING)
    logger.info("Elasticsearch index name {} was created".format(folder_index))

    # inserts first datum structure
    logger.info("Starting matching & indexing process on the input folder: ".format(folder_path))
    with ThreadPoolExecutor() as pool:
        file_paths = [os.path.join(root, file) for root, dirs, files in os.walk(folder_path) for file in files]
        datum_generator = pool.map(partial(process_file, es_client=es_client,
                                           project_vic_index=vic_index_name),
                                   file_paths)

        insert_es_data(datum_generator, folder_index, es_client)
    logger.info("Finished matching & indexing process on the input folder: ".format(folder_path))


def apply_ml_process(model_path, gpu_count, process, es_client, folder_index):
    logger.info(f"Starting ml process: {process} on {folder_index} index")
    path_to_id = {item["path"]: item["id"] for item in
                  get_file_paths_generator_from_index(es_client, folder_index)}
    image_paths = list(path_to_id.keys())
    outputs = infer(image_paths=image_paths,
                    model_path=model_path,
                    gpu_count=gpu_count,
                    process=process)
    ml_objs = [{"id": path_to_id[key],
                process: val} for key, val in outputs.items()]
    logger.info(f"Finished ml process: {process} on {folder_index} index")

    logger.info(f"Starting indexing {process} items into the {folder_index} index")
    if process == IMAGE_CLASSIFICATION:
        update_es_classification(ml_objs, folder_index, es_client)
    elif process == FEATURE_EXTRACTION:
        update_es_fv(ml_objs, folder_index, es_client)
    else:
        raise ValueError(f"{process} is not supported")
    logger.info(f"Finished indexing {process} items into the {folder_index} index")


def apply_face_detection_process(es_client, folder_index):
    faces = get_faces_bb(es_client, folder_index)
    logger.debug("Starting indexing faces items into the {} index".format(folder_index))
    update_es_faces(faces, folder_index, es_client)
    logger.debug("Finished indexing faces items into the {} index".format(folder_index))


def apply_estimation_process(process, folder_path, crop, gpu_count, es_client, folder_index):
    faces_to_predict = get_faces(es_client, folder_index)
    logger.info(f"Starting {process} estimation on the input folder: {folder_path}")
    if process == AGE:
        age_gender_gen = get_age_gender(faces_to_predict, AGE, AGE_MODEL_PATH, gpu_count, crop)
    else:  # p == GENDER:
        age_gender_gen = get_age_gender(faces_to_predict, GENDER, GENDER_MODEL_PATH, gpu_count, crop)
    logger.info(f"Finished {process} estimation on the input folder: {folder_path}")

    logger.info(f"Start indexing {process} estimation results for the input folder: {folder_path}")
    update_age_gender_index(es_client, folder_index, age_gender_gen, AGES if process == AGE else GENDERS)
    logger.info(f"Finished indexing {process} estimation results for the input folder: {folder_path}")


def process_folder(folder_path, folder_index, processes=None, crop=None):
    """
    A function which process all kind of ML analytics on an input folder.
    :param folder_path: the full path of the input folder we wish to prcess
    :param folder_index: the name of the index for the process folder
    :param processes: type of process - age/gender/face/image_classification/feature_extraction
    :param crop: for age/gender process - single crop or multi-crop inference
    :return:
    """
    # define variables which are taken the config.ini
    vic_index_name = CSA_CONFIG["project_vic"]["index"]
    server, port = CSA_CONFIG["database"]["server"], CSA_CONFIG["database"]["port"]
    model_path = os.path.join(MODELS_PATH, CSA_CONFIG["models"]["classification_model_relpath"])
    gpu_count = int(CSA_CONFIG["models"]["gpu_count"])

    # in case index does not exists run the whole process
    es_client = ES(f"http://{server}:{port}")

    if not es_client.indices.exists(index=folder_index):
        create_index(folder_path, es_client, vic_index_name, folder_index)
        create_dashboard(folder_index,
                         visualization_json_path=IDX_VISUALIZTIONS_JSON,
                         dashboard_json_path=IDX_DASHBOARD_JSON,
                         nested=True)

    for p in processes:
        if p in {IMAGE_CLASSIFICATION, FEATURE_EXTRACTION}:
            subprocess(apply_ml_process)(model_path, gpu_count, p, es_client, folder_index)

        if p == FACE:
            subprocess(apply_face_detection_process)(es_client, folder_index)

        if p in (AGE, GENDER):
            subprocess(apply_estimation_process)(p, folder_path, crop, gpu_count, es_client, folder_index)


def export_folder_as_catalouge(index, classes):
    server, port = CSA_CONFIG["database"]["server"], CSA_CONFIG["database"]["port"]

    # in case index does not exists run the whole process
    es_client = ES(f"http://{server}:{port}")

    if classes == "vic":
        data = {cls: [] for cls in VIC_CATEGORIES_MAP}
        paths = []
        for i, item in enumerate(get_vic_matches(es_client=es_client, index_name=index)):
            paths.append(item['path'])
            category = item["vic_category"]
            for cat, labels in data.items():
                if cat != category:
                    labels.append(0)
                else:
                    labels.append(1)

        data['file name'] = paths
        catalogue = pd.DataFrame.from_dict(data)

    else:  # classes == "estimate"
        csa_paths = {item["path"] for item in get_csa_images(es_client=es_client, index_name=index)}
        data = {"file name": [], "csa":[]}
        for item in get_file_paths_generator_from_index(es_client=es_client, index_name=index):
            path = item['path']
            data['file name'].append(path)
            data['csa'].append(0 if path not in csa_paths else 1)

        catalogue = pd.DataFrame.from_dict(data)

    return catalogue
