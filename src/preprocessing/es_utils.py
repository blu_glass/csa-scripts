from elasticsearch.helpers import scan, bulk
from config.consts import AGES, GENDERS, FACES, IMAGE_CLASSIFICATION, FEATURE_EXTRACTION
from tqdm import tqdm


def _update_es_data(datum_generator,
                    datum_key,
                    script,
                    index_name,
                    es_client):
    """
    :param datum_generator: generates documents for elastic update op  - each document is a dict, and must contain id 
    and datum_key fields
    :param doc_key: name of document field we want to update
    :param datum_key: datum key to update in the document field <doc_key>
    :param index_name: index that we want to update
    :param es_client: es_client
    :return: 
    """
    actions = ({
        "_op_type": "update",
        "_index": index_name,
        "_type": "doc",
        "_id": datum['id'],
        "script": {
            "source": script,
            "params": {
                datum_key: datum[datum_key]
            }
        }
    } for datum in datum_generator)
    bulk(es_client, tqdm(actions))


def update_es_fv(fv_generator, index_name, es_client):
    script = f"if (ctx._source.{FEATURE_EXTRACTION} != null)" \
             f"{{ctx._source.{FEATURE_EXTRACTION}.add(params.{FEATURE_EXTRACTION})}}" \
             f" else {{ctx._source['{FEATURE_EXTRACTION}'] = [params.{FEATURE_EXTRACTION}]}}"
    return _update_es_data(datum_generator=fv_generator,
                           datum_key=FEATURE_EXTRACTION,
                           script=script,
                           index_name=index_name,
                           es_client=es_client)


def update_es_classification(classification_generator, index_name, es_client):
    script = f"if (ctx._source.{IMAGE_CLASSIFICATION} != null)" \
             f"{{ctx._source.{IMAGE_CLASSIFICATION}.addAll(params.{IMAGE_CLASSIFICATION})}}" \
             f" else {{ctx._source['{IMAGE_CLASSIFICATION}'] = params.{IMAGE_CLASSIFICATION}}}"
    return _update_es_data(datum_generator=classification_generator,
                           datum_key=IMAGE_CLASSIFICATION,
                           script=script,
                           index_name=index_name,
                           es_client=es_client)


def update_es_faces(faces_generator, index_name, es_client):
    actions = ({
        "_op_type": "update",
        "_index": index_name,
        "_type": "doc",
        "_id": datum['id'],
        "doc": {FACES: datum[FACES]}
    } for datum in faces_generator)
    bulk(es_client, tqdm(actions))


def insert_es_data(datum_generator, index_name, es_client):
    """
        :param doc_count:
        :param datum_generator:
        :param index_name:
        :param es_client:
        :return:
        """
    actions = ({
        "_index": index_name,
        "_type": "doc",
        "_id": item['Hash']['MD5'],
        "_source": item
    } for item in datum_generator)
    bulk(es_client, tqdm(actions))


def match_to_project_vic(file_md5, file_photodna, es_client, project_vic_index):
    """
    A function which check if an input file is a match in the project vic
    :param file_md5: input file md5 hash
    :param es_client: the Elasticsearch client
    :param project_vic_index: index name we want to search on
    :return: list containing the search result
    """
    try:
        res = es_client.search(index=project_vic_index,
                               body={
                                   "query": {
                                       "bool": {
                                           "should": [
                                               {"term": {"Hash.MD5": file_md5}},
                                               {"term": {"PhotoDNA": file_photodna}}
                                           ]
                                       }
                                   }
                               })

        if res['hits']['total'] >= 1:
            doc = res[0]['_source']

            vic_dict = {
                "in_project_vic": True,
                "category": doc['Category'],
            }

            # check if the item has tags
            if len(doc['Tags']):
                vic_dict["Tags"] = doc['Tags']

            photo_dna = doc["Hash"].get("PhotoDNA", None)
            if photo_dna:
                vic_dict["PhotoDNA"] = photo_dna

        else:
            vic_dict = {"in_project_vic": False,
                        "category": "No match"}

        return vic_dict

    except Exception as e:
        print(e)
        return None


def get_faces(es_client, index_name, source=None):
    source = source if source is not None else []

    for entry in scan(es_client,
                      index=index_name,
                      query={"_source": ["path", "faces"] + source,
                             "query": {"bool": {"must": [{"term": {"is_corrupted": "false"}},
                                                         {"nested": {"path": "faces",
                                                                     "query": {"exists": {"field": "faces"}}}}]}}}):
        res = {"id": entry['_id'],
               FACES: entry['_source'][FACES],
               "path": entry['_source']['path']}

        if source:
            res.update({src: entry["_source"][src] for src in source})

        yield res


def get_vic_matches(es_client, index_name):
    for entry in scan(es_client,
                      index=index_name,
                      query={"_source": ["path", "vic_info"],
                             "query": {"bool": {"filter": {"term": {"vic_info.in_project_vic": "true"}}}}}):
        yield {"id": entry['_id'],
               "path": entry['_source']['path'],
               "vic_category": entry['_source']['vic_info']["category"]}


def get_csa_images(es_client, index_name, age_groups=None):
    age_groups = age_groups if age_groups is not None else ['child', 'teen']
    if len(age_groups) > 1:
        age_group_filter = {
            "bool": {
                "should": [{
                    "nested": {
                        "path": "ages",
                        "query": {"bool": {"must": [{"term": {"ages.group": grp}}]}}}
                } for grp in age_groups]}}
    else:
        age_group_filter = {
            "nested": {
                "path": "ages",
                "query": {"bool": {"must": [{"term": {"ages.group": age_groups[0]}}]}}
            }}

    query = {"bool": {"must": [
        age_group_filter,
        {
            "nested": {
                "path": "classification",
                "query": {"bool": {"must": [{"term": {"classification.category": "Nudity"}}]}}}
        }]}}

    for entry in scan(es_client,
                      index=index_name,
                      query={"query": query}):
        yield {"id": entry['_id'],
               FACES: entry['_source'][FACES],
               GENDERS: entry['_source'][GENDERS],
               AGES: entry['_source'][AGES],
               "path": entry['_source']['path']}


def get_file_paths_generator_from_index(es_client, index_name):
    for entry in scan(es_client,
                      index=index_name,
                      query={"_source": ["path"],
                             "query": {"bool": {"filter": {"term": {"is_corrupted": "false"}}}}}):
        yield {"path": entry['_source']['path'], "id": entry['_id']}


def update_age_gender_index(es_client, index_name, id_to_age_gender, label):
    actions = ({
        "_op_type": "update",
        "_index": index_name,
        "_type": "doc",
        "_id": _id,
        "doc": {label: items}
    } for _id, items in id_to_age_gender.items())
    bulk(es_client, tqdm(actions))
