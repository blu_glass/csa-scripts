import logging.config
import os
import configparser

ROOT_PATH = os.path.abspath(os.path.join(__file__, os.pardir, os.pardir, os.pardir))
SRC_PATH = os.path.join(ROOT_PATH, "src")
RESOURCES_PATH = os.path.join(ROOT_PATH, "resources")

MODELS_PATH = os.path.join(RESOURCES_PATH, 'models')
CONFIG_PATH = os.path.join(SRC_PATH, 'csa_config.ini')

CSA_CONFIG = configparser.ConfigParser(allow_no_value=True)
with open(CONFIG_PATH) as f:
    CSA_CONFIG.read_string(f.read())

LOGS_DIR = os.path.join(ROOT_PATH, 'logs')
if not os.path.exists(LOGS_DIR):
    os.mkdir(LOGS_DIR)
LOGS_PATH = os.path.join(LOGS_DIR, CSA_CONFIG["handler_csa"]["args"][0])
logging.config.fileConfig(CONFIG_PATH)

# models path
AGE_MODEL_PATH = os.path.join(MODELS_PATH, "age")
GENDER_MODEL_PATH = os.path.join(MODELS_PATH, "gender")
FACE_MODEL_PATH = os.path.join(MODELS_PATH, "face")

# kibana json config
KIBANA_RESOURCES_PATH = os.path.join(RESOURCES_PATH, "kibana")
VIC_DASHBOARD_JSON = os.path.join(KIBANA_RESOURCES_PATH, os.path.sep.join(["vic_json", "dashboard.json"]))
VIC_VISUALIZTIONS_JSON = os.path.join(KIBANA_RESOURCES_PATH, os.path.sep.join(["vic_json", "visualizations.json"]))

IDX_DASHBOARD_JSON = os.path.join(KIBANA_RESOURCES_PATH, os.path.sep.join(["index_json", "dashboard.json"]))
IDX_VISUALIZTIONS_JSON = os.path.join(KIBANA_RESOURCES_PATH, os.path.sep.join(["index_json", "visualizations.json"]))

# photo_dna path
PHOTODNA_LIB_PATH = os.path.join(RESOURCES_PATH, "bin")
