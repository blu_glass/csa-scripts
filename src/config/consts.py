# es mapping field names
AGES = "ages"
GENDERS = "genders"
FACES = "faces"

# processes names
AGE = "age"
GENDER = "gender"
FACE = "face"
IMAGE_CLASSIFICATION = "classification"
FEATURE_EXTRACTION = "fv"
PROCESSES = [IMAGE_CLASSIFICATION, FEATURE_EXTRACTION, FACE, AGE, GENDER]