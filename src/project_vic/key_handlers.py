from project_vic.config import VIC_CATEGORIES_MAP
import logging

logger = logging.getLogger("csa")


def category_handler(item):
    item['Category'] = VIC_CATEGORIES_MAP[item['Category']]


def isdistributed_handler(item):
    item['IsDistributed'] = item['IsDistributed'].lower()


def victimidentified_handler(item):
    identified_handler('VictimIdentified', item)


def offenderidentified_handler(item):
    identified_handler('OffenderIdentified', item)


def identified_handler(key, item):
    if "Identified" in item:
        item['Identified'][key[:-10]] = item[key].lower()

    else:
        item['Identified'] = {
            key[:-10]: item[key].lower(),
        }

    del item[key]


def tags_handler(item):
    tags_dict = []
    tags_names = item["Tags"].replace("#", "").split(',')  # TODO: validate this split, check tags convention
    for tag in tags_names:
        tags_dict.append(tag)
    item["Tags"] = tags_dict


def md5_handler(item):
    hash_handler('MD5', item['MD5'], item)
    del item['MD5']


def sha1_handler(item):
    hash_handler('SHA1', item['SHA1'], item)
    del item['SHA1']


def alternativehashes_handler(item):
    for hash in item['AlternativeHashes']:
        hash_handler(hash['HashName'], hash["HashValue"], item)

    del item["AlternativeHashes"]


def hash_handler(hash_name, hash_val, item):
    if "Hash" in item:
        item["Hash"][hash_name] = hash_val

    else:
        item["Hash"] = {hash_name: hash_val}


SUPPRTED_KEYS_HANDLERS = {'OffenderIdentified': offenderidentified_handler,
                          'MD5': md5_handler,
                          'SHA1': sha1_handler,
                          'IsDistributed': isdistributed_handler,
                          'Tags': tags_handler,
                          'VictimIdentified': victimidentified_handler,
                          'Category': category_handler,
                          'AlternativeHashes': alternativehashes_handler}
