from tqdm import tqdm
import uuid

import ijson
from elasticsearch import Elasticsearch as ES, helpers
from elasticsearch.exceptions import RequestError
import logging
from config.resources import CSA_CONFIG
from project_vic.config import MAPPING, VIC_CATEGORIES_MAP, SUPPORTED_KEYS
from project_vic.key_handlers import SUPPRTED_KEYS_HANDLERS

logger = logging.getLogger("csa")


def vic_item_generator(vic_items, index_name):
    """
    Creates an es action generator for the es indexing
    :param vic_items: list of vic itmes
    :param index_name: the index name we wish to insert to vic items
    :return: index action generator
    """
    unhandled_keys = set()
    for item in tqdm(vic_items):
        # convert boolean string to lower case
        for key, value in list(item.items()):
            if key in SUPPRTED_KEYS_HANDLERS:
                SUPPRTED_KEYS_HANDLERS[key](item)
            elif key in SUPPORTED_KEYS:
                continue
            else:
                unhandled_keys.add(key)
                del item[key]

        # the es object item
        action = {
            "_index": index_name,
            "_type": "doc",
            "_id": uuid.uuid4(),
            "_source": item
        }
        yield action

    logger.debug(f"The following keys was not handled {str(unhandled_keys)[1:-1]}")


def vic_parser(vic_json_path, es_client, index_name):
    """
    A json parser for the project vic db.
    this function parse each item and index it to the Elastic Search
    :param vic_json_path: the project vic db file (which it is a json file)
    :param es_client: the elastic search client
    :param index_name: the name of the index we wish to insert the project vic items
    :return:
    """
    with open(vic_json_path, 'rb') as input_file:
        # load json iteratively
        logger.debug("Starting the json parsing")
        actions = vic_item_generator(ijson.items(input_file, 'value.item'), index_name)
        logger.debug("Finished the json parsing")

        logger.debug("Starting indexing items into the {} index".format(index_name))
        # inserts the data into the elastic search
        helpers.bulk(es_client, actions, chunk_size=500)
        logger.debug("Finished indexing items into the {} index".format(index_name))


def import_vic_to_es(vic_json_path, vic_index_name):
    """
    Main function which parse and uploads project vic db into the elastic search index
    :param vic_json_path: the project vic db file (which it is a json file)
    """

    # define variables which are taken the config.ini
    server, port = CSA_CONFIG["database"]["server"], CSA_CONFIG["database"]["port"]
    es_client = ES("http://{}:{}".format(server, port))

    try:
        es_client.indices.create(index=vic_index_name, body=MAPPING)
    except RequestError:
        logger.error("index name {} already exists".format(vic_index_name))
        exit(1)

    logger.info("Elasticsearch index name {} was created".format(vic_index_name))
    vic_parser(vic_json_path, es_client, vic_index_name)

