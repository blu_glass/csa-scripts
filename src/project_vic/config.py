# CONSTANTS
MAPPING = {
    "mappings": {
        "doc": {
            "properties": {
                "Category": {"type": "keyword"},
                "DateUpdated": {"type": "date"},
                "IsDistributed": {"type": "boolean"},
                "MediaID": {"type": "long"},
                "MediaSize": {"type": "long"},
                "Identified": {
                    "type": "object",
                    "properties": {
                        "Offender": {"type": "boolean"},
                        "Victim": {"type": "boolean"}
                    }
                },
                "Hash": {
                    "type": "object",
                    "dynamic": "true",
                    "properties": {
                        "SHA1": {"type": "keyword"},
                        "MD5": {"type": "keyword"},
                        "PhotoDNA": {"type": "keyword"}
                    }
                },
                "Series": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "Tags": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "odata": {
                    "type": "object",
                    "properties": {
                        "id": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 256
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

SUPPORTED_KEYS = set(MAPPING['mappings']['doc']['properties'].keys())

# project vic categories. each index refers to the category index in the hash set
VIC_CATEGORIES_MAP = ["Non-Pertinent",
                      "Child Abuse Material",
                      "Child Exploitive / Age Difficult",
                      "CGI/Animation - Child Exploitive"
                      "Comparison Images",
                      "Uncategorized"]