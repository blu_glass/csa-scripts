# https://github.com/yeephycho/tensorflow-face-detection

# -*- coding: utf-8 -*-
# pylint: disable=C0103
# pylint: disable=E1101
import os
import numpy as np
import tensorflow as tf
import cv2

from preprocessing.es_utils import get_file_paths_generator_from_index
from .utils import label_map_util
from config.resources import FACE_MODEL_PATH

# Path to frozen detection graph. This is the actual model that is used for the object detection.
CKPT_PATH = os.path.join(FACE_MODEL_PATH, "frozen_inference_graph_face.pb")
LABELS_PATH = os.path.join(FACE_MODEL_PATH, "face_label_map.pbtxt")
NUM_CLASSES = 2

# List of the strings that is used to add correct label for each box.
label_map = label_map_util.load_labelmap(LABELS_PATH)
categories = label_map_util.convert_label_map_to_categories(label_map,
                                                            max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)


class TensoflowFaceDector(object):
    def __init__(self, PATH_TO_CKPT):
        """Tensorflow detector
        """

        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.io.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        with self.detection_graph.as_default():
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True
            self.sess = tf.Session(graph=self.detection_graph, config=config)
            self.windowNotSet = True

    def run(self, image):
        """image: bgr image
        return (boxes, scores, classes, num_detections)
        """
        image_np = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # the array based representation of the image will be used later in order to prepare the
        # result image with boxes and labels on it.
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)
        image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
        # Actual detection.
        (boxes, scores, classes, num_detections) = self.sess.run(
            [boxes, scores, classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})
        return boxes, scores, classes, num_detections


def get_faces_bb(es_client, folder_index):
    tDetector = TensoflowFaceDector(CKPT_PATH)

    # runs over file in the folder
    for item in list(get_file_paths_generator_from_index(es_client, folder_index)):
        file_path = item["path"]
        # defines variables
        result = {"id": item["id"], "faces": []}
        image = cv2.imread(file_path)
        if image is not None:
            h, w, c = image.shape

            (boxes, scores, classes, num_detections) = tDetector.run(image)
            for i in range(100):
                if scores[0][i] > 0.7:
                    l, t, r, b = int(boxes[0][i][1] * w),\
                                 int(boxes[0][i][0] * h),\
                                 int(boxes[0][i][3] * w),\
                                 int(boxes[0][i][2] * h)
                    result["faces"].append({"left": l, "top": t, "right": r, "bottom": b})
            yield result
