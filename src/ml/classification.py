import json
import os

from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True

from keras import Model
from keras.engine.saving import load_model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import multi_gpu_model

from config.consts import FEATURE_EXTRACTION, IMAGE_CLASSIFICATION
from preprocessing.utils import convert_gen_to_dataframe
# import numpy as np


def imagenet_preprocess(x):
    x /= 127.5
    x -= 1
    return x


def mobilenetV2_preprocess(x):
    x /= 128
    x -= 1
    return x


def densenet_preprocess(x):
    x /= 255.
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    x[..., 0] -= mean[0]
    x[..., 1] -= mean[1]
    x[..., 2] -= mean[2]
    if std is not None:
        x[..., 0] /= std[0]
        x[..., 1] /= std[1]
        x[..., 2] /= std[2]

    return x


# def alexnet_preprocess(x):
    # combos = new PreProcCombo[6]
    # combos[0] = new PreProcCombo(false, 231, 0.5f, new CropStart(0, 4))
    # combos[1] = new PreProcCombo(mirror=true, resize=284, view=0.5f, new CropStart(0, 57))
    # combos[2] = new PreProcCombo(false, 231, 0.0f, new CropStart(0, 4))
    # combos[3] = new PreProcCombo(false, 231, 0.5f, new CropStart(4, 4))
    # combos[4] = new PreProcCombo(false, 284, 1.0f, new CropStart(0, 57))
    # combos[5] = new PreProcCombo(false, 256, 0.0f, new CropStart(29, 29))


PREPROCESS_DICT = {
    'MobileNetV2': mobilenetV2_preprocess,
    'MobileNet': imagenet_preprocess,
    'ResNet50': imagenet_preprocess,
    'NASNetMobile': imagenet_preprocess,
    'NASNetLarge': imagenet_preprocess,
    'VGG19': imagenet_preprocess,
    'VGG16': imagenet_preprocess,
    'InceptionV3': imagenet_preprocess,
    'InceptionResNetV2': imagenet_preprocess,
    'Xception': imagenet_preprocess,
    'DenseNet121': densenet_preprocess,
    'DenseNet169': densenet_preprocess,
    'DenseNet201': densenet_preprocess,
    # 'Alexnet': alexnet_preprocess
}


# def normalize_score(scores, intercept, slope):
#     odds = np.exp(intercept + slope * scores)
#     return odds / (1 + odds)


def keras_infer(img_frame,
                img_dir: str,
                model,
                preprocessing_function,
                target_size=(224, 224)):
    data_gen = ImageDataGenerator(preprocessing_function=preprocessing_function)

    data_generator = data_gen.flow_from_dataframe(dataframe=img_frame,
                                                  directory=img_dir,
                                                  x_col='filename',
                                                  y_col='label',
                                                  target_size=target_size,
                                                  batch_size=1)

    predictions = model.predict_generator(data_generator,
                                          max_queue_size=16,
                                          workers=7,
                                          steps=data_generator.samples,
                                          use_multiprocessing=False,
                                          verbose=1)

    results = {os.path.join(img_dir, path): score
               for path, score in zip(data_generator.filenames, predictions)}
    return results


def infer(image_paths, model_path, gpu_count, process):
    """
    A function which ingest a folder of images and extract their feature vectors
    :param model_name: ml model
    :param image_paths:
    :param model_path: path of the model we want to use
    :param gpu_count: in case we have gpu, how many to use
    :return: key value of an image and feature vector
    """
    with open(os.path.join(os.path.dirname(model_path), "meta.json"), 'r') as f:
        model_meta = json.load(f)
    model_name = model_meta["name"]

    # models prep
    model = load_model(model_path, compile=False)
    if process == FEATURE_EXTRACTION:
        model = Model(inputs=model.input, outputs=model.layers[model_meta["fv_layer"]].input)
    if gpu_count > 1:
        model = multi_gpu_model(model, gpu_count)

    img_frame = convert_gen_to_dataframe(image_paths)
    outputs = keras_infer(img_frame, "", model, PREPROCESS_DICT[model_meta["arch"]], model_meta["input shape"])

    if process == IMAGE_CLASSIFICATION:
        # if "intercept" in model_meta:
        #     keys = list(outputs.keys())
        #     norm_scores = normalize_score(scores=np.asarray(list(outputs.values())),
        #                                   intercept=model_meta['intercept'],
        #                                   slope=model_meta['slope'])
        #     outputs = {k: v for k, v in zip(keys, norm_scores)}

        inv_class_indices = {val: key for key, val in model_meta["class_indices"].items()}
        thresholds = model_meta['thresholds']
        result = {key: [{"model": model_name,
                         "category": inv_class_indices[i],
                         "prob": float(v)} for i, v in enumerate(val) if v > thresholds[i]]
                  for key, val in outputs.items()}
    elif process == FEATURE_EXTRACTION:
        result = {key: {"model": model_name,
                        FEATURE_EXTRACTION: val.tolist()} for key, val in outputs.items()}
    else:
        raise ValueError(f"{process} is not supported")

    return result
