# https://github.com/dpressel/rude-carnie
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from config.consts import AGE
from .model import get_checkpoint, inception_v3
from .utils import *
import csv
from tqdm import tqdm

CHECKPOINT = 'checkpoint'
HARDWARE = {0: "/cpu:0", 1: "/device:GPU:0"}

RESIZE_FINAL = 227
MAX_BATCH_SZ = 16
GENDER_LIST = ['Male', 'Female']
# https://talhassner.github.io/home/projects/Adience/Adience-data.html#agegender
AGE_LIST = ['(0, 2)', '(4, 6)', '(8, 12)', '(15, 20)', '(25, 32)', '(38, 43)', '(48, 53)', '(60, 100)']
AGE_GROUP = {'(0, 2)': "child",
             '(4, 6)': "child",
             '(8, 12)': "child",
             '(15, 20)': "teen",
             '(25, 32)': "adult",
             '(38, 43)': "adult",
             '(48, 53)': "adult",
             '(60, 100)': "elder"}


def one_of(fname, types):
    return any([fname.endswith('.' + ty) for ty in types])


def aggregate_by_id(age_gender_gen, label):
    age_gender_dict = {}
    for file_id, age_gender, prob in age_gender_gen:
        if label == AGE:
            item = {"age": age_gender,
                    "probability": float(prob),
                    "group": AGE_GROUP[age_gender]}
        else:  # label == GENDER
            item = {"gender": age_gender,
                    "probability": float(prob)}

        age_gender_dict.setdefault(file_id, []).append(item)
    return age_gender_dict


def classify_many_single_crop(sess, label_list, softmax_output, coder, images, image_files):
    try:
        num_batches = math.ceil(len(image_files) / MAX_BATCH_SZ)
        for j in range(num_batches):
            start_offset = j * MAX_BATCH_SZ
            end_offset = min((j + 1) * MAX_BATCH_SZ, len(image_files))

            batch_image_files = image_files[start_offset:end_offset]
            print(start_offset, end_offset, len(batch_image_files))

            start = time.time()
            image_batch = make_multi_image_batch(batch_image_files, coder)
            end = time.time()
            print("to run multi_batch : " + str((end - start)))

            start = time.time()
            batch_results = sess.run(softmax_output, feed_dict={images: image_batch.eval(session=sess)})
            end = time.time()
            print("to run session : " + str((end - start)))

            batch_sz = batch_results.shape[0]
            for i in range(batch_sz):
                output_i = batch_results[i]
                best_i = np.argmax(output_i)
                # best_choice = (label_list[best_i], output_i[best_i])
                yield label_list[best_i], output_i[best_i], batch_image_files[i]
    except Exception as e:
        print(e)
        print('Failed to run all images')


def classify_one_multi_crop(sess, label_list, softmax_output, coder, images, images_files):
    for image_file in images_files:
        try:

            image_batch = make_multi_crop_batch(image_file, coder)

            start = time.time()
            batch_results = sess.run(softmax_output, feed_dict={images: image_batch.eval(session=sess)})
            end = time.time()
            print("to run session : " + str((end - start)))

            output = batch_results[0]
            batch_sz = batch_results.shape[0]

            for i in range(1, batch_sz):
                output = output + batch_results[i]

            output /= batch_sz
            best = np.argmax(output)

            yield image_file, label_list[best], output[best]

        except Exception as e:
            print(e)
            print('Failed to run image %s ' % image_file)


def classify_one_multi_crop_nd(sess, label_list, softmax_output, images, images_files):
    for image_file in tqdm(images_files):
        try:
            batch_results = sess.run(softmax_output, feed_dict={images: image_file[1]})

            output = batch_results.mean(axis=0)
            best = np.argmax(output)

            yield image_file[0], label_list[best], output[best]

        except Exception as e:
            print(e)
            print('Failed to run image %s ' % image_file)


def list_images(srcfile):
    with open(srcfile, 'r') as csvfile:
        delim = ',' if srcfile.endswith('.csv') else '\t'
        reader = csv.reader(csvfile, delimiter=delim)
        if srcfile.endswith('.csv') or srcfile.endswith('.tsv'):
            print('skipping header')
            _ = next(reader)

        return [row[0] for row in reader]


def get_faces_files_2(faces_to_predict, method):
    input_files = []
    for item in faces_to_predict:
        path = item["path"]
        for face in item["faces"]:
            img = cv2.imread(path)
            input_files.append([item["id"], sub_image_2(method,
                                                        img,
                                                        face["left"],
                                                        face["top"],
                                                        face["right"] - face["left"],
                                                        face["bottom"] - face["top"])])
    return input_files


def get_age_gender(faces_to_predict, class_type, model_path, gpu_count, method):
    flag = HARDWARE[gpu_count]

    input_files = get_faces_files_2(faces_to_predict, method)

    label_list = AGE_LIST if class_type == AGE else GENDER_LIST
    nlabels = len(label_list)
    model_fn = inception_v3

    with tf.device(flag):
        config = tf.ConfigProto(allow_soft_placement=True)
        sess = tf.Session(config=config)

        # build & load model
        images = tf.placeholder(tf.float32, [None, RESIZE_FINAL, RESIZE_FINAL, 3])
        stand_imgs = standardize_image(images)
        logits = model_fn(nlabels, stand_imgs, 1, False)

        model_checkpoint_path, global_step = get_checkpoint(model_path, None, CHECKPOINT)
        saver = tf.train.Saver()
        saver.restore(sess, model_checkpoint_path)

        softmax_output = tf.nn.softmax(logits)

        age_gender_gen = classify_one_multi_crop_nd(sess, label_list, softmax_output, images, input_files)

        return aggregate_by_id(age_gender_gen, class_type)
