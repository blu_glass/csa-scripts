from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import six.moves
from datetime import datetime
import sys
import math
import time

from .data import inputs, standardize_image
import numpy as np
import tensorflow as tf
import re
# from .dlibdetect import FaceDetectorDlib
import cv2

RESIZE_AOI = 256
RESIZE_FINAL = 227
MULTI = "multi_crop"


# Modifed from here
# http://stackoverflow.com/questions/3160699/python-progress-bar#3160819

# Read image files
class ImageCoder(object):

    def __init__(self):
        # Create a single Session to run all image coding calls.
        config = tf.ConfigProto(allow_soft_placement=True)
        self._sess = tf.Session(config=config)

        # Initializes function that converts PNG to JPEG data.
        self._png_data = tf.placeholder(dtype=tf.string)
        image = tf.image.decode_png(self._png_data, channels=3)
        self._png_to_jpeg = tf.image.encode_jpeg(image, format='rgb', quality=100)

        # Initializes function that decodes RGB JPEG data.
        self._decode_jpeg_data = tf.placeholder(dtype=tf.string)
        self._decode_jpeg = tf.image.decode_jpeg(self._decode_jpeg_data, channels=3)
        self.crop = tf.image.resize_images(self._decode_jpeg, (RESIZE_AOI, RESIZE_AOI))

    def png_to_jpeg(self, image_data):
        return self._sess.run(self._png_to_jpeg,
                              feed_dict={self._png_data: image_data})

    def decode_jpeg(self, image_data):
        image = self._sess.run(self.crop,  # self._decode_jpeg,
                               feed_dict={self._decode_jpeg_data: image_data})

        assert len(image.shape) == 3
        assert image.shape[2] == 3
        return image


def _is_png(filename):
    """Determine if a file contains a PNG format image.
    Args:
    filename: string, path of the image file.
    Returns:
    boolean indicating if the image is a PNG.
    """
    return '.png' in filename


def make_batch(image, coder, multicrop):
    """Process a single image file."""

    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 100]
    result, img_str = cv2.imencode('.jpg', image, encode_param)
    #
    image = coder.decode_jpeg(img_str.tostring(order='C'))
    crops = []

    if multicrop is False:
        print('Running a single image')
        crop = tf.image.resize_images(image, (RESIZE_FINAL, RESIZE_FINAL))
        image = standardize_image(crop)

        crops.append(image)
    else:
        print('Running multi-cropped image')
        h = image.shape[0]
        w = image.shape[1]
        hl = h - RESIZE_FINAL
        wl = w - RESIZE_FINAL

        crop = tf.image.resize_images(image, (RESIZE_FINAL, RESIZE_FINAL))
        crops.append(standardize_image(crop))
        crops.append(tf.image.flip_left_right(crop))

        corners = [(0, 0), (0, wl), (hl, 0), (hl, wl), (int(hl / 2), int(wl / 2))]
        for corner in corners:
            ch, cw = corner
            cropped = tf.image.crop_to_bounding_box(image, ch, cw, RESIZE_FINAL, RESIZE_FINAL)
            crops.append(standardize_image(cropped))
            flipped = tf.image.flip_left_right(cropped)
            crops.append(standardize_image(flipped))

    image_batch = tf.stack(crops)

    return image_batch


def make_multi_image_batch(filenames, coder):
    """Process a multi-image batch, each with a single-look
    Args:
    filenames: list of paths
    coder: instance of ImageCoder to provide TensorFlow image coding utils.
    Returns:
    image_buffer: string, JPEG encoding of RGB image.
    """

    images = []
    #
    for filename in filenames:

        with tf.io.gfile.FastGFile(filename, 'rb') as f:
            image_data = f.read()
        # Convert any PNG to JPEG's for consistency.
        if _is_png(filename):
            print('Converting PNG to JPEG for %s' % filename)
            image_data = coder.png_to_jpeg(image_data)

        start = time.time()
        image = coder.decode_jpeg(image_data)
        end = time.time()
        print("to run decode : " + str((end - start)))

        crop = tf.image.resize_images(image, (RESIZE_FINAL, RESIZE_FINAL))
        image = standardize_image(crop)
        images.append(image)
    image_batch = tf.stack(images)
    return image_batch


def make_multi_crop_batch(filename, coder):
    """Process a single image file.
    Args:
    filename: string, path to an image file e.g., '/path/to/example.JPG'.
    coder: instance of ImageCoder to provide TensorFlow image coding utils.
    Returns:
    image_buffer: string, JPEG encoding of RGB image.
    """
    # Read the image file.
    with tf.io.gfile.FastGFile(filename, 'rb') as f:
        image_data = f.read()

    # Convert any PNG to JPEG's for consistency.
    if _is_png(filename):
        print('Converting PNG to JPEG for %s' % filename)
        image_data = coder.png_to_jpeg(image_data)

    image = coder.decode_jpeg(image_data)

    crops = []
    h = image.shape[0]
    w = image.shape[1]
    hl = h - RESIZE_FINAL
    wl = w - RESIZE_FINAL

    crop = tf.image.resize_images(image, (RESIZE_FINAL, RESIZE_FINAL))
    crops.append(standardize_image(crop))
    crops.append(standardize_image(tf.image.flip_left_right(crop)))

    corners = [(0, 0), (0, wl), (hl, 0), (hl, wl), (int(hl / 2), int(wl / 2))]
    for corner in corners:
        ch, cw = corner
        cropped = tf.image.crop_to_bounding_box(image, ch, cw, RESIZE_FINAL, RESIZE_FINAL)
        crops.append(standardize_image(cropped))
        flipped = standardize_image(tf.image.flip_left_right(cropped))
        crops.append(standardize_image(flipped))

    image_batch = tf.stack(crops)
    return image_batch


def make_multi_crop_nd(roi_color):
    crops = []

    # decoding
    img = cv2.resize(roi_color, dsize=(RESIZE_AOI, RESIZE_AOI), interpolation=cv2.INTER_CUBIC)
    h = img.shape[0]
    w = img.shape[1]
    hl = h - RESIZE_FINAL
    wl = w - RESIZE_FINAL

    crop = cv2.resize(img, dsize=(RESIZE_FINAL, RESIZE_FINAL), interpolation=cv2.INTER_CUBIC)
    crops.append(crop)
    crops.append(cv2.flip(crop, 0))

    corners = [(0, 0), (0, wl), (hl, 0), (hl, wl), (int(hl / 2), int(wl / 2))]
    for corner in corners:
        ch, cw = corner
        cropped = img[ch:ch + RESIZE_FINAL, cw:cw + RESIZE_FINAL]
        crops.append(cropped)
        flipped = cv2.flip(cropped, 0)
        crops.append(flipped)

    return np.stack(crops)


def sub_image(name, img, x, y, w, h):
    FACE_PAD = int(min(w / 2, h / 2))
    upper_cut = [min(img.shape[0], y + h + FACE_PAD), min(img.shape[1], x + w + FACE_PAD)]
    lower_cut = [max(y - FACE_PAD, 0), max(x - FACE_PAD, 0)]
    roi_color = img[lower_cut[0]:upper_cut[0], lower_cut[1]:upper_cut[1]]
    cv2.imwrite(name, roi_color)
    return name


def sub_image_2(method, img, x, y, w, h):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    FACE_PAD = int(min(w / 2, h / 2))
    upper_cut = [min(img.shape[0], y + h + FACE_PAD), min(img.shape[1], x + w + FACE_PAD)]
    lower_cut = [max(y - FACE_PAD, 0), max(x - FACE_PAD, 0)]
    roi_color = img[lower_cut[0]:upper_cut[0], lower_cut[1]:upper_cut[1]]

    if method == MULTI:
        return make_multi_crop_nd(roi_color)
    else:
        return [cv2.resize(roi_color, dsize=(RESIZE_FINAL, RESIZE_FINAL), interpolation=cv2.INTER_CUBIC)]
