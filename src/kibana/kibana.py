import logging

from config.resources import CSA_CONFIG
from kibana.api import add_index_pattern, add_visualizations, add_dashboard
from kibana.utils import load_visualization_json, load_dashboard_json

logger = logging.getLogger("csa")


def create_dashboard(index_name,
                     dashboard_json_path,
                     visualization_json_path,
                     nested):
    """
    This script is in charge of creating the dashboard and the visualizations for the input elasticsearch index
    The input index represents the project vic database
    :return:
    """
    # define variables which are taken the config.ini
    server, port = CSA_CONFIG["database"]["server"], CSA_CONFIG["database"]["port"]

    logger.info("Started creating kibana dashboard")

    # creates the project vic index pattern
    vic_index_id = add_index_pattern(index_name, nested, server)

    # creates the visualizations for the project vic index
    visualiztions_list = load_visualization_json(visualization_json_path, vic_index_id)
    vis_title_to_id = add_visualizations(visualiztions_list, server)
    # creates the project vic dashboard
    dashboard_prop = load_dashboard_json(dashboard_json_path, index_name, vis_title_to_id)
    add_dashboard(dashboard_prop, server)

    logger.info("Finished creating project vic dashboard")
