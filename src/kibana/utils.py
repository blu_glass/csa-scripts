import json
from string import Template


def load_visualization_json(json_path, index_id):
    """
    A fucntions which generates the json visualization of Kibana for
    the project vic
    :param json_path: the default json path of the kibana visualization
    :param index_id: the index id of the vic json
    :return: json file containing the index visualization
    """
    with open(json_path, 'r') as input_file:
        template_str = input_file.read()

    template = Template(template_str)
    json_str = template.substitute(index_id=index_id)
    visualiztions_list = json.loads(json_str)

    return visualiztions_list


def load_dashboard_json(json_path, index_name, vis_title_to_id):
    """
    A function which generates the dashboard json for the Kibana
    :param vis_title_to_id: visualiztions title to elastic id map
    :param json_path: the default json path of the kibana dashboard
    :param index_name: the index name of the vic json
    :return: json file containing the index visualization
    """
    # make sure placeholders names does not contain spaces
    vis_title_to_id = {k.replace(" ", "_"): v for k, v in vis_title_to_id.items()}

    with open(json_path, 'r') as input_file:
        template_str = input_file.read()

    template = Template(template_str)
    json_str = template.substitute(index_name=index_name, **vis_title_to_id)
    dashboard_json = json.loads(json_str)

    return dashboard_json