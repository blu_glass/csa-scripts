import requests
import logging

logger = logging.getLogger("csa")


def add_index_pattern(index_name, nested, es_host):
    """
    A function that creates the index pattern of the project vic index
    :param es_host: elasticsearch host ip
    :param index_name: the name of the es index
    :return: return the index pattern id
    """
    # creates the index pattern
    response = requests.post(f"http://{es_host}:5601/api/saved_objects/index-pattern",
                             json={"attributes": {"title": "".join([index_name, "*"])}},
                             headers={'content-type': "application/json", 'kbn-xsrf': "reporting"})
    if not response.ok:
        logger.error(f"Failed to create the index {index_name} pattern")
        exit(1)

    # response = requests.put(f"http://{es_host}:5601/api/saved_objects/index-pattern/{response.json()['id']}",
    #                         json={"attributes": {"nested": nested}},
    #                         headers={'content-type': "application/json", 'kbn-xsrf': "reporting"})

    return response.json()['id']


def add_visualizations(list_of_visual, es_host):
    """
    A function which Creates and import the project vic visualizations into the kibana index pattern
    :param list_of_visual: list of the default configurations of the visualizations
    :param es_host: elasticsearch host ip
    """
    title_to_id = {}
    # iterates over the defaults visualizations and updates them into the kibana
    for visual in list_of_visual:
        response = requests.post(f"http://{es_host}:5601/api/saved_objects/visualization?overwrite=true",
                                 json={"attributes": visual['_source']},
                                 headers={'content-type': "application/json", 'kbn-xsrf': "reporting"})
        if not response.ok:
            logger.error(f"Failed to to add visualization {visual['_source']['title']}")
        else:
            response_dict = response.json()
            title_to_id[response_dict['attributes']['title']] = response_dict["id"]

    return title_to_id


def add_dashboard(dashboard_prop, es_host):
    """
    A function which Creates and import the project vic dashboard into the kibana index pattern
    :param dashboard_prop: default configurations of the dashboard
    :param es_host: elasticsearch host ip
    :return:
    """
    response = requests.post(f"http://{es_host}:5601/api/saved_objects/dashboard?overwrite=true",
                             json={"attributes": dashboard_prop["_source"]},
                             headers={'content-type': "application/json", 'kbn-xsrf': "reporting"})
    if not response.ok:
        logging.error(f'Failed to create the index {dashboard_prop["_source"]["title"]} dashboard')
        exit(1)

    return response.json()['id']
