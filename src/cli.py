import warnings
warnings.filterwarnings('ignore', category=FutureWarning)

import os
from click import option, group, Choice

from config.resources import CSA_CONFIG, VIC_VISUALIZTIONS_JSON, VIC_DASHBOARD_JSON, RESOURCES_PATH
from preprocessing.folder_preprocess import process_folder, export_folder_as_catalouge
from config.consts import PROCESSES
from project_vic.project_vic_es_loader import import_vic_to_es
from kibana.kibana import create_dashboard

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

@group()
def main():
    pass


@main.command()
@option("--path", type=str, required=True, help="path to folder containing images")
@option("--index", type=str, default=None, required=False, help="elastic index name")
@option("--processes", "-p",
        type=Choice(PROCESSES + ['all'], case_sensitive=False),
        default=CSA_CONFIG["processes"]["process"],
        multiple=True,
        required=False,
        help="type of process to execute")
@option("--crop",
        type=Choice(["single", "multi"]),
        default=CSA_CONFIG["processes"]["crop"],
        required=False,
        help="for age/gender process - single crop or multi-crop inference")
def apply_process(path, index, processes, crop):
    path = os.path.abspath(path)
    index = index.lower() if index is not None else os.path.basename(path).lower()
    processes = PROCESSES if "all" in processes else processes
    process_folder(folder_path=path,
                   folder_index=index,
                   processes=processes,
                   crop=crop)


@main.command()
@option("--path", type=str, required=True, help="path to project vic json")
def import_vic(path):
    path = os.path.abspath(path)
    vic_index_name = CSA_CONFIG["project_vic"]["index"]
    import_vic_to_es(vic_json_path=path, vic_index_name=vic_index_name)
    create_dashboard(vic_index_name,
                     visualization_json_path=VIC_VISUALIZTIONS_JSON,
                     dashboard_json_path=VIC_DASHBOARD_JSON,
                     nested=False)


@main.command()
@option("--index", type=str, required=True, help="elastic index name")
@option("--export_path", type=str, default=None, required=False, help="where to save the catalogue")
@option("--classes", "-c", type=Choice(['vic', 'estimate'], case_sensitive=False),
        default='vic', required=False, help="use vic classes, or estimated csa class")
def export_catalogue(index, export_path, classes):
    if export_path is None:
        export_path = os.path.join(RESOURCES_PATH, f"{index}_{classes}_catalogue.csv")
    catalogue = export_folder_as_catalouge(index, classes)
    catalogue.to_csv(export_path, index=False)


if __name__ == '__main__':
    main()
