import ctypes
import sys
import base64
from PIL import Image
import numpy as np

#Allow any image size
Image.MAX_IMAGE_PIXELS = None

# RobustHashing functions [SEE LICENSE]
from .RobustHashTools import PhotoDNA, HASH_SIZE, init_photodna

#Handle PIL logging errors for < python v3
if sys.version_info[0] < 3:
  import logging
  class LogHandlerPIL(logging.Handler):
      def emit(self, record):
          pass
  logging.getLogger('PIL.ImageFile').addHandler(LogHandlerPIL())

#Allow any image size
Image.MAX_IMAGE_PIXELS = None

# any imagetype that is supported by the pillow library can be added to this list
IMG_TYPES = {".jpg", ".png", ".jpeg", ".gif", ".bmp", ".tiff"}
TXT_TYPES = {".txt", ".lst", ".text", ".list"}
USE_BASE64 = True
HASH_DTYPE = np.uint8

fatalerror = False

init_photodna()

# <summary>
#      Pack numpy array into a CSV string.
# </summary>
# <param name="hashvalue">array of 8 bit unsigned integers</param>
# <returns>CSV string</returns>
def hash_to_string(hashdata, Base64=USE_BASE64):
    """Pack numpy array into a CSV string."""
    if Base64:
        return base64.b64encode(hashdata).decode('utf-8')
    else:
        out = ""
        for i in hashdata:
            out += str(i) + ","
        return out[0:-1]


def string_to_hash(hashstr, is_base64=USE_BASE64):
    """
    convert hash strin from csv-like or base64, to numpy array
    :param hashstr: 
    :param is_base64: 
    :return: 
    """""
    if is_base64:
        return np.frombuffer(base64.b64decode(hashstr), dtype=np.uint8)
    else:
        return np.fromstring(hashstr, sep=',', dtype=np.uint8)


# <summary>
#      Compute the hash for the given image.
# </summary>
# <param name="filename">filename of image</param>
# <returns>CSV string</returns>
def calculate_hash(filename, rh, use_base64=USE_BASE64):
    """Compute the hash for the given image."""
    output = ""
    img = Image.open(filename)
    arr = np.array(img.convert('RGB'))
    hash_value = np.zeros(HASH_SIZE, dtype=np.uint8)  # array to hold hash

    res = rh.ComputeHash(ctypes.c_void_p(arr.ctypes.data),        # RGB array
                         img.size[0],                             # Width
                         img.size[1],                             # Height
                         0,                                       # Stride
                         ctypes.c_void_p(hash_value.ctypes.data))  # hash array
    if res == 0:
        output = hash_to_string(hash_value, use_base64)
    del img
    del arr

    return output


# scan the input item by item
def scanfile(filename, rhv, use_base64=USE_BASE64):
    if not fatalerror:
        result = ""
        filetype = filename.split(".")[-1].lower()  # file extension

        if "." + filetype in IMG_TYPES:  # An image file
            try:
                result = calculate_hash(filename, rhv, use_base64)
            except Exception as e:
                print(e)
            if len(result) > 0:
                return result
            else:
                print(filename + ": Invalid file, failed to create hash")
        else:
            print(filename + ": file type is not supported")

        return result


def calculate_photo_dna(file_name):
    rhval = PhotoDNA(1920*1080)
    return scanfile(file_name, rhval)
