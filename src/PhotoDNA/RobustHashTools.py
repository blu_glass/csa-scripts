################################################################################
# 
# IMPORTANT NOTICE
# ================
# 
#   Copyright (C) 2016, 2017, Microsoft Corporation
#   All Rights Reserved.
# 
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS STRICTLY UNDER THE TERMS
#   OF A SEPARATE LICENSE AGREEMENT.  NO SOURCE CODE DISTRIBUTION OR SOURCE 
#   CODE DISCLOSURE RIGHTS ARE PROVIDED WITH RESPECT TO THIS SOFTWARE OR ANY 
#   DERIVATIVE WORKS THEREOF.  USE AND NON-SOURCE CODE REDISTRIBUTION OF THE 
#   SOFTWARE IS PERMITTED ONLY UNDER THE TERMS OF SUCH LICENSE AGREEMENT.  
#   THIS SOFTWARE AND ANY WORKS/MODIFICATIONS/TRANSLATIONS DERIVED FROM THIS 
#   SOFTWARE ARE COVERED BY SUCH LICENSE AGREEMENT AND MUST ALSO CONTAIN THIS 
#   NOTICE.  THIS SOFTWARE IS CONFIDENTIAL AND SENSITIVE AND MUST BE SECURED 
#   WITH LIMITED ACCESS PURSUANT TO THE TERMS OF SUCH LICENSE AGREEMENT.
# 
#   THE SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
#   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
#   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#   COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
#   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
#   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#   OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
#   WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
#   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
#   ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#   
#   Project:       PhotoDNA Robust Hashing
#   File:          RobustHashTools.py
#   Description:   Matching and utility functions for Python samples
#
#   History:  
#     2015.06.05   adrian chandley  Created Python example.
#     2015.12.09   adrian chandley  Added Base64 support and table output format.
#     2016.05.06   adrian chandley  Created a wrapper class to simplify use.
#
################################################################################
"""Matching and utility functions for Python samples"""
import base64
import sys
import os
import ctypes
import numpy as np

from config.resources import PHOTODNA_LIB_PATH

HASH_SIZE = 144
SHORT_HASH_SIZE = 64
CHECK_SIZE = 36


def setupDLLfunctions(PhotoDNA):
    # RobustHashInitBuffer(initialSize):
    #     <i>Optional:</i> The PhotoDNA hash calculation requires the use of temporary buffers,
    #     if you chose not to pre-allocate memory then temporary allocation will be
    #     made each time the ComputeRobustHash function is called which will impact
    #     performance when multiple hashes are being calculated.
    #     initialSize: The initial size for the image buffer. The buffer will be
    #         resized if a larger image is encountered.
    #   Returns: Reference to the allocated buffer
    #
    PhotoDNA._PdnaRobustHashInitBuffer = PhotoDNA._DllPDNA.RobustHashInitBuffer
    PhotoDNA._PdnaRobustHashInitBuffer.restype = ctypes.c_void_p
    PhotoDNA._PdnaRobustHashInitBuffer.argtypes = [
        ctypes.c_int]

    # RobustHashReleaseBuffer(dataBuffer):
    #     <i>Optional:</i> If you chose to pre-allocate a temporary buffer by calling RobustHashInitBuffer,
    #     then that memory is released using this function.
    #     dataBuffer *: Reference to the previously allocated buffer.
    #
    PhotoDNA._PdnaRobustHashReleaseBuffer = PhotoDNA._DllPDNA.RobustHashReleaseBuffer
    PhotoDNA._PdnaRobustHashReleaseBuffer.restype = None
    PhotoDNA._PdnaRobustHashReleaseBuffer.argtypes = [
        ctypes.c_void_p]

    # RobustHashResetBuffer(dataBuffer):
    #     <i>Optional:</i> If you chose to allocate a temporary buffer by calling RobustHashInitBuffer,
    #     then that memory may be reset back to its initial size using this
    #     function.
    #     dataBuffer *: Reference to the previously allocated buffer.
    #
    PhotoDNA._PdnaRobustHashResetBuffer = PhotoDNA._DllPDNA.RobustHashResetBuffer
    PhotoDNA._PdnaRobustHashResetBuffer.restype = None
    PhotoDNA._PdnaRobustHashResetBuffer.argtypes = [
        ctypes.c_void_p]

    # ComputeRobustHash(imageData, width, height, stride, hashValue, dataBuffer):
    #     Computes the RobustHash of a image. The image should be passed as an RGB
    #     byte array in raster read order.
    #     imageData *: Pointer to the image pixels.
    #         The size of buffer should be stride*height or width*height*3 if stride is 0.
    #         The pixels are stored in raster scan order, with each pixel having a red,
    #         green and blue value.
    #     width: Width of the image in pixels
    #     height: Height of the image in pixels
    #     stride: Stride (row length) of the image in bytes,
    #         or 0 if this should be calculated from the image dimensions
    #     hashValue *: byte array to return hash in
    #     dataBuffer *: Buffer allocated using RobustHashInitBuffer If dataBuffer pointer is NULL,
    #         then a buffer will be allocated for the duration of this call.
    #   Returns: status: 0 if call completed successfully,
    #     an error number if not. Please refer to RobustHash.h for a list of error
    #     numbers.
    #
    PhotoDNA._PdnaComputeRobustHash = PhotoDNA._DllPDNA.ComputeRobustHash
    PhotoDNA._PdnaComputeRobustHash.restype = ctypes.c_int
    PhotoDNA._PdnaComputeRobustHash.argtypes = [
        ctypes.c_void_p,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_void_p,
        ctypes.c_void_p]

    # ComputeRobustHashAltColor(imageData, width, height, stride, color_type,
    #     hashValue, dataBuffer):
    #     Computes the RobustHash of an image specifying a color format type.
    #     imageData *: Pointer to the image pixels. The size of buffer will be
    #         dependent on the image color format
    #     width: Width of the image in pixels
    #     height: Height of the image in pixels
    #     stride: Stride (row length) of the image in bytes,
    #         or 0 if this should be calculated from the image dimensions
    #     color_type: 0 = RGB, 1 = RGBA, 2 = Grey8, 4 = YUV420p
    #     hashValue *: byte array to return hash in (or NULL)
    #     dataBuffer *: Buffer allocated using RobustHashInitBuffer If dataBuffer pointer is NULL,
    #         then a buffer will be allocated for the duration of this call.
    #   Returns: status: 0 if call completed successfully,
    #     an error number if not. Please refer to RobustHash.h for a list of error
    #     numbers.
    #
    PhotoDNA._PdnaComputeRobustHashAltColor = PhotoDNA._DllPDNA.ComputeRobustHashAltColor
    PhotoDNA._PdnaComputeRobustHashAltColor.restype = ctypes.c_int
    PhotoDNA._PdnaComputeRobustHashAltColor.argtypes = [
        ctypes.c_void_p,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_void_p,
        ctypes.c_void_p]

    # ComputeRobustHashBorder(imageData, width, height, stride, color_type, border,
    #     hashValue, hashValueTrimmed, dataBuffer):
    #     Computes the RobustHash of an image with optional border detection.
    #     This function can return the hash value for the original image
    #     and the hash value for the image with any border removed.
    #     If both forms of hashes are required should a border be present,
    #     then it will be substantially faster to request both hashes in a single
    #     call to this function.
    #     imageData *: Pointer to the image pixels. The size of buffer will be
    #         dependent on the image color format
    #     width: Width of the image in pixels
    #     height: Height of the image in pixels
    #     stride: Stride (row length) of the image in bytes,
    #         or 0 if this should be calculated from the image dimensions
    #     color_type: 0 = RGB, 1 = RGBA, 2 = Grey8, 4 = YUV420p
    #     border *: If border detection and removal is required,
    #         pass a pointer to a return array (of 4 x 32bit values) or NULL,
    #         null or None for no border detection and removal. If the hashvalue pointers are both NULL,
    #         then only the border dimensions will be returned.
    #     hashValue *: byte array to return hash in (or NULL)
    #     hashValueTrimmed *: byte array to return the trimmed hash in or NULL,
    #         if provided and no border is found then the content of hashValue and
    #         hashValueTrimmed will be the same
    #     dataBuffer *: Buffer allocated using RobustHashInitBuffer If dataBuffer pointer is NULL,
    #         then a buffer will be allocated for the duration of this call.
    #   Returns: status: 0 if call completed successfully,
    #     an error number if not. Please refer to RobustHash.h for a list of error
    #     numbers.
    #
    PhotoDNA._PdnaComputeRobustHashBorder = PhotoDNA._DllPDNA.ComputeRobustHashBorder
    PhotoDNA._PdnaComputeRobustHashBorder.restype = ctypes.c_int
    PhotoDNA._PdnaComputeRobustHashBorder.argtypes = [
        ctypes.c_void_p,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_void_p,
        ctypes.c_void_p,
        ctypes.c_void_p,
        ctypes.c_void_p]

    # ComputeRobustHashAdvanced(imageData, width, height, stride, color_type, sub_x,
    #     sub_y, sub_w, sub_h, border, hashValue, hashValueTrimmed, dataBuffer):
    #     Computes the RobustHash of an image,
    #     this advance function allows the caller to specify an inner part of the image to be hashed
    #     as well as providing optional border detection.
    #     If border selection is required,
    #     this function can return the hash value for the original image
    #     and also the hash value for the image with any border removed.
    #     If both forms of hashes are required should a border be present,
    #     then it will be substantially faster to request both hashes in a single
    #     call to this function.
    #     imageData *: Pointer to the image pixels. The size of buffer will be
    #         dependent on the image color format
    #     width: Width of the image in pixels
    #     height: Height of the image in pixels
    #     stride: Stride (row length) of the image in bytes,
    #         or 0 if this should be calculated from the image dimensions
    #     color_type: 0 = RGB, 1 = RGBA, 2 = Grey8, 4 = YUV420p
    #     sub_x: X position of image to be selected in pixels
    #     sub_y: Y position of image to be selected in pixels
    #     sub_w: Width of image to be selected in pixels
    #     sub_h: Heightof image to be selected in pixels
    #     border *: If border detection and removal is required,
    #         pass a pointer to a return array (of 4 x 32bit values) or NULL,
    #         null or None for no border detection and removal. If the hashvalue pointers are both NULL,
    #         then onlythe border dimensions will be returned.
    #     hashValue *: byte array to return hash in (or NULL)
    #     hashValueTrimmed *: byte array to return the trimmed hash in or NULL,
    #         if provided and no border is found then the content of hashValue and
    #         hashValueTrimmed will be the same
    #     dataBuffer *: Buffer allocated using RobustHashInitBuffer If dataBuffer pointer is NULL,
    #         then a buffer will be allocated for the duration of this call.
    #   Returns: status: 0 if call completed successfully,
    #     an error number if not. Please refer to RobustHash.h for a list of error
    #     numbers.
    #
    PhotoDNA._PdnaComputeRobustHashAdvanced = PhotoDNA._DllPDNA.ComputeRobustHashAdvanced
    PhotoDNA._PdnaComputeRobustHashAdvanced.restype = ctypes.c_int
    PhotoDNA._PdnaComputeRobustHashAdvanced.argtypes = [
        ctypes.c_void_p,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_void_p,
        ctypes.c_void_p,
        ctypes.c_void_p,
        ctypes.c_void_p]

    # ComputeShortHash(imageData, width, height, stride, color_type, shortHashValue,
    #     dataBuffer):
    #     Computes a Short Hash for a given input byte array which represents an image in raster read order.
    #     Unlike the full hash which comprises 144 bytes, the Short hash contains 64 bytes.
    #     The hash is calculated from the center
    #     two thirds of the image (w * 2/3 x h 2/3) and is approximately 2x faster
    #     than computing the full hash.
    #     imageData *: Pointer to the image pixels. The size of buffer should be
    #         stride*height or width*height*3 if stride is 0.
    #     width: Width of the image in pixels
    #     height: Height of the image in pixels
    #     stride: Stride (row length) of the image in bytes,
    #         or 0 if this should be calculated from the image dimensions
    #     color_type: 0 = RGB, 1 = RGBA, 2 = Grey8, 4 = YUV420p
    #     shortHashValue *:
    #     dataBuffer *: Buffer allocated using RobustHashInitBuffer If dataBuffer pointer is NULL,
    #         then a buffer will be allocated for the duration of this call.
    #   Returns: status: 0 if call completed successfully,
    #     an error number if not. Please refer to RobustHash.h for a list of error
    #     numbers.
    #
    PhotoDNA._PdnaComputeShortHash = PhotoDNA._DllPDNA.ComputeShortHash
    PhotoDNA._PdnaComputeShortHash.restype = ctypes.c_int
    PhotoDNA._PdnaComputeShortHash.argtypes = [
        ctypes.c_void_p,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_void_p,
        ctypes.c_void_p]

    # ComputeCheckArray(imageData, width, height, stride, color_type, checkValue,
    #     dataBuffer):
    #     Computes a 36 byte Check array for a given image. A check array is intended to allow a very fast method to determine if images are different. Not suitable for matching images,
    #     however is substantially faster than computing and comparing PhotoDNA
    #     hashes.
    #     imageData *: Pointer to the image pixels. The size of buffer will be
    #         dependent on the image color format
    #     width: Width of the image in pixels
    #     height: Height of the image in pixels
    #     stride: Stride (row length) of the image in bytes,
    #         or 0 if this should be calculated from the image dimensions
    #     color_type: 0 = RGB, 1 = RGBA, 2 = Grey8, 4 = YUV420p
    #     checkValue *: byte array to return Check data in
    #     dataBuffer *: Buffer allocated using RobustHashInitBuffer If dataBuffer pointer is NULL,
    #         then a buffer will be allocated for the duration of this call.
    #   Returns: status: 0 if call completed successfully,
    #     an error number if not. Please refer to RobustHash.h for a list of error
    #     numbers.
    #
    PhotoDNA._PdnaComputeCheckArray = PhotoDNA._DllPDNA.ComputeCheckArray
    PhotoDNA._PdnaComputeCheckArray.restype = ctypes.c_int
    PhotoDNA._PdnaComputeCheckArray.argtypes = [
        ctypes.c_void_p,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_void_p,
        ctypes.c_void_p]

    # RobustHashVersion():
    #     Determine library version number.
    #   Returns: Library version number as 32 bits,
    #     the upper 8 bits are the major version number,
    #     the next 8 bits are the minor version number. The lower 16 bits is a
    #     build reference. e.g. Version 1.70 ref 17125 would be returned as
    #     0x014642e5
    #
    PhotoDNA._PdnaRobustHashVersion = PhotoDNA._DllPDNA.RobustHashVersion
    PhotoDNA._PdnaRobustHashVersion.restype = ctypes.c_int
    PhotoDNA._PdnaRobustHashVersion.argtypes = []


def init_photodna():
    if PhotoDNA._DllPDNA is None:
        PhotoDNA._dllpath = PHOTODNA_LIB_PATH
        # x64 or x86
        PhotoDNA._dllname = r"PhotoDNAx64" if (sys.maxsize >> 30) > 1 else r"PhotoDNAx86"

        try:
            # arm64 or arm32
            if (os.uname()[4][:5] == "armv7") or (os.uname()[4][:5] == "aarch"):
                PhotoDNA._dllname = r"PhotoDNAarm64" if (sys.maxsize >> 30) > 1 else r"PhotoDNAarm32"
        except Exception:
            pass

        # determine location of library from OS and script directory
        if sys.platform[0:3].lower() == "win":  # windows or osx/linux
            PhotoDNA._dllname = PhotoDNA._dllname + r".1.72.dll"
        else:
            PhotoDNA._dllname = PhotoDNA._dllname + r".so.1.72"
        # Load the library into memory. Try source path, then default
        #
        path = os.path.join(PhotoDNA._dllpath, PhotoDNA._dllname)

        try:  # Path + name
            PhotoDNA._DllPDNA = ctypes.cdll.LoadLibrary(path)
            setupDLLfunctions(PhotoDNA)
            print(f"PhotoDNA version {version_bytes_to_str(PhotoDNA.HashVersion())} loaded successfully")
        except Exception as e:
            print(f"Failed to load PhotoDNA")
            raise e


# <summary>
#      Wrapper class to retain the DLL functions and buffers
# </summary>
# <param name="startsize">Initial buffer size in pixels</param>
# <returns>0 for success</returns>
class PhotoDNA(object):
    """Wrapper class to retain the DLL functions and buffers"""
    _DllPDNA = None

    def __init__(self, startsize=0):
        self._buffers = PhotoDNA._PdnaRobustHashInitBuffer(startsize)

    def __del__(self):
        PhotoDNA._PdnaRobustHashReleaseBuffer(self._buffers)

    def ComputeHash(self, imageData, width, height, stride, hashValue):
        """Computes the RobustHash of a image. The image should be passed as an RGB byte array in raster read order."""
        return PhotoDNA._PdnaComputeRobustHash(imageData, width, height, stride, hashValue, self._buffers)

    def ComputeHashAltColor(self, imageData, width, height, stride, color_type, hashValue):
        """Computes the RobustHash of an image specifying a color format type."""
        return PhotoDNA._PdnaComputeRobustHashAltColor(imageData, width, height, stride, color_type, hashValue,
                                                       self._buffers)

    def ComputeHashBorder(self, imageData, width, height, stride, color_type, border, hashValue, hashValueTrimmed):
        """Computes the RobustHash of an image with optional border detection. This function can return the hash
        value for the original image and the hash value for the image with any border removed. If both forms of
        hashes are required should a border be present, then it will be substantially faster to request both hashes
        in a single call to this function. """
        return PhotoDNA._PdnaComputeRobustHashBorder(imageData, width, height, stride, color_type, border, hashValue,
                                                     hashValueTrimmed, self._buffers)

    def ComputeHashAdvanced(self, imageData, width, height, stride, color_type, sub_x, sub_y, sub_w, sub_h, border,
                            hashValue, hashValueTrimmed):
        """Computes the RobustHash of an image, this advance function allows the caller to specify an inner part of
        the image to be hashed as well as providing optional border detection. If border selection is required,
        this function can return the hash value for the original image and also the hash value for the image with any
        border removed. If both forms of hashes are required should a border be present, then it will be
        substantially faster to request both hashes in a single call to this function. """
        return PhotoDNA._PdnaComputeRobustHashAdvanced(imageData, width, height, stride, color_type, sub_x, sub_y,
                                                       sub_w, sub_h, border, hashValue, hashValueTrimmed, self._buffers)

    def ComputeShort(self, imageData, width, height, stride, color_type, shortHashValue):
        """Computes a Short Hash for a given input byte array which represents an image in raster read order. Unlike
        the full hash which comprises 144 bytes, the Short hash contains 64 bytes. The hash is calculated from the
        center two thirds of the image (w * 2/3 x h 2/3) and is approximately 2x faster than computing the full hash.
        """
        return PhotoDNA._PdnaComputeShortHash(imageData, width, height, stride, color_type, shortHashValue,
                                              self._buffers)

    def ComputeCheck(self, imageData, width, height, stride, color_type, checkValue):
        """Computes a 36 byte Check array for a given image. A check array is intended to allow a very fast method to
        determine if images are different. Not suitable for matching images, however is substantially faster than
        computing and comparing PhotoDNA hashes. """
        return PhotoDNA._PdnaComputeCheckArray(imageData, width, height, stride, color_type, checkValue, self._buffers)

    @classmethod
    def HashVersion(cls):
        """Determine library version number."""
        return PhotoDNA._PdnaRobustHashVersion()


# <summary>
#      Compares 2 hashes and returns the square of the euclidean distance.
# </summary>
# <param name="hash1">First hash to compare</param>
# <param name="hash1">Second hash to compare</param>
# <returns>Square of the euclidean distance between hashes</returns>
def HashDistance(hash1, hash2):
    """Compares 2 hashes and returns the square of the euclidean distance."""
    if str(hash1.dtype)[0:1] == 'u':
        return np.sum((hash1.astype(np.uint16) - hash2) ** 2)
    else:
        return np.sum((hash1 - hash2) ** 2)


# <summary>
#      Finds the closest match for a hash in a database of hashes.
# </summary>
# <param name="hash">Hash to match</param>
# <param name="hashdb">Database (array) of hashes to be searched</param>
# <returns>Matching DB entry number, Square of the euclidean distance between
#          hashes
# </returns>
def FindMatch(searchhash, hashdb):
    """Finds the closest match for a hash in a database of hashes."""
    distance = 0
    closest = -1
    if str(searchhash.dtype)[0:1] == 'u':
        hash1 = searchhash.astype(np.uint16)
    else:
        hash1 = searchhash
    for i in range(len(hashdb)):
        res = np.sum((hash1 - hashdb[i]) ** 2)
        if (closest == -1) or (distance > res):
            closest = i
            distance = res
    return [closest, distance]


# <summary>
#      Finds the first match for a hash in a database of hashes.
# </summary>
# <param name="hash">Hash to match</param>
# <param name="hashdb">Database (array) of hashes to be searched</param>
# <param name="target">Target value below which a match is be returned</param>
# <returns>Matching DB entry number, Square of the euclidean distance between 
#          hashes
# </returns>
def FindFirstMatch(searchhash, hashdb, target):
    """Finds the first match for a hash in a database of hashes."""
    distance = 0
    res = -1
    if str(searchhash.dtype)[0:1] == 'u':
        hash1 = searchhash.astype(np.uint16)
    else:
        hash1 = searchhash
    for i in range(len(hashdb)):
        dist = np.sum((hash1 - hashdb[i]) ** 2)
        if dist <= target:
            res = i
            distance = dist
            break
    return [res, distance]


# <summary>
#      Reads hashes from a given file of hashes.
#      File should contains lines in the format:
#      <name>,<optional>,<..>,<optional>,<comma seperate hash value>
# </summary>
# <param name="path">Filename containing hashes</param>
# <param name="dtype">Type of variable (np.uint8 if sharing with C++)</param>
# <returns>Array hashes read</returns>
def ReadHashList(path, dtype=np.uint16):
    """Reads hashes from a given file of hashes."""
    names = []
    hashes = []
    FORMAT_CSV = 1
    FORMAT_BASE64 = 2
    datatype = 0
    hashoffset = 0
    for line in open(path, 'r'):
        line = line.rstrip().replace('\t', ',')  # replace tabs with commas
        if datatype == 0:
            count = line.count(',')
            if count < 1:
                continue  # no data on line
            elif count < HASH_SIZE:  # hash data is base64
                datatype = FORMAT_BASE64
                hashoffset = count
            else:  # hash data is CSV
                datatype = FORMAT_CSV
                hashoffset = count - (HASH_SIZE - 1)
        first = line.find(',')
        pos = first + 1
        for _ in range(hashoffset - 1):
            pos = line.find(',', pos) + 1
        if pos == -1:
            continue  # bad line, ignore
        if datatype == FORMAT_CSV:
            row = np.fromstring(line[pos:], dtype=dtype, sep=',')
        else:
            row = np.array(map(ord, base64.b64decode(line[pos:])), dtype=dtype)
        if row.shape[0] != HASH_SIZE:
            continue  # bad line, ignore
        hashes.append(row)
        names.append(line[0:first])
    return [names, hashes]


def version_bytes_to_str(version_number):
    """
    :param version_number: version number as returned from RobustHashVersion func
    :return: parsed version number as a string, as specified in RobustHashVersion
    """
    ver_bytes = int.to_bytes(version_number, length=4, byteorder='big')
    return f"{ver_bytes[0]}.{ver_bytes[1]}.{int.from_bytes(ver_bytes[2:], byteorder='big')}"
